<?php
/**
 * A couple test cases that we're using to destroy applicants :)
 *
 * @author tylerc <tylerc@upsync.com>
 */
class ProjectTest extends PHPUnit_Framework_TestCase
{
    /*
     * @var str $gistFile The filename of the gist test.
     */
     protected $gistFile = "albatross.txt";

    /**
     * runs before all other tests are run
     *
     * @author tylerc <tylerc@upsync.com>
     * 
     * @return void
     */
    protected function setup()
    {
        require_once __DIR__ . '/../TextFile.php';
    }

    /**
     * test functions that read text files
     *
     * @author tylerc <tylerc@upsync.com>
     * 
     * @return void
     */
    public function testRead()
    {
        $tf = new TextFile;

        // textfile functions being called for the test
        $resultOne = $tf->getDate();
        $resultTwo = $tf->getBar();
        $resultThree = $tf->getWideface();
        $resultFour = $tf->getCopycat();

        $this->assertEquals(file_get_contents(__DIR__ . '/../fonts-jokes/date.txt'), $resultOne);
        $this->assertEquals(file_get_contents(__DIR__ . '/../fonts-jokes/bar.txt'), $resultTwo);
        $this->assertEquals(file_get_contents(__DIR__ . '/../fonts-jokes/wideface.txt'), $resultThree);
        $this->assertEquals(file_get_contents(__DIR__ . '/../fonts-jokes/copycat.txt'), $resultFour);
    }

    /**
     * Test pulling the gist content from github.com via the API.
     *
     * @author JohnG <john.gieselmann@upsync.com>
     *
     * @return void
     */
    public function testGist()
    {
        $tf = new TextFile;

        //gist tests
        $gistPath = $tf->getGist();

        $this->assertFileExists(__DIR__ . "/../riddle_me_this.txt"); 
        $this->assertFileEquals(__DIR__ . "/../gist/{$this->gistFile}", $gistPath);
    }
}
